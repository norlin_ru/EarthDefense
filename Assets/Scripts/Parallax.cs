﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {
	public Camera currentCamera;
	public float parallax = 0.9f;

	void Update() {
		Vector3 cameraPosition = currentCamera.transform.position;
		this.transform.position = new Vector3(cameraPosition.x, cameraPosition.y, 0) * parallax;
	}
}
