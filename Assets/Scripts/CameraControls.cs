﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {
	public float cameraSpeed = 10;
	public float cameraReturnSpeed = 0.7f;
	public float limitX = 10;
	public float limitY = 5;

	void FixedUpdate() {
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");

		bool snapToOrigin = false;

		float cameraX = this.transform.position.x;
		float cameraY = this.transform.position.y;

		if (inputY == 0 && inputX == 0) {
			snapToOrigin = true;

			if (cameraX != 0) {
				inputX = -1 * Mathf.Sign(cameraX) * Mathf.Max(Mathf.Sign(cameraX) * cameraX / limitX, cameraReturnSpeed);
			}

			if (cameraY != 0) {
				inputY = -1 * Mathf.Sign(cameraY) * Mathf.Max(Mathf.Sign(cameraY) * cameraY / limitY, cameraReturnSpeed);
			}
		}

		float newPositionX = Mathf.Clamp(cameraX + inputX * cameraSpeed, -limitX, limitX);
		float newPositionY = Mathf.Clamp(cameraY + inputY * cameraSpeed, -limitY, limitY);

		if (snapToOrigin) {
			if (inputX > 0 && newPositionX > 0) {
				newPositionX = Mathf.Clamp(newPositionX, -limitX, 0);
			} else if (inputX < 0 && newPositionX < 0) {
				newPositionX = Mathf.Clamp(newPositionX, 0, limitX);
			}

			if (inputY > 0 && newPositionY > 0) {
				newPositionY = 0;
			} else if (inputY < 0 && newPositionY < 0) {
				newPositionY = 0;
			}
		}

		this.transform.position = new Vector3(newPositionX, newPositionY, -10);
	}
}
